from asyncio.windows_events import NULL
from msilib.schema import Error
from multiprocessing.sharedctypes import Value
import os
import time
from random import randrange
import pyttsx3
import speech_recognition as sr


r = sr.Recognizer()
engine = pyttsx3.init()
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[2].id)

def listen()->int:
    with sr.Microphone() as source:
        print('Speak anything: ')
        audio = r.listen(source)
        try:
            text = r.recognize_google(audio, language='fr-FR')
            print('You said : {0} '.format(int(text)))
            return int(text)
        except :
            return -1

def talk(sentence):
    engine.say(sentence)
    engine.runAndWait()

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')


def exam(size, mode='classic'):
    print(f"Recall {size} numbers in the given order")
    time.sleep(2.0)
    clear()
    
    print("Get Ready 🟥🟥🟥")
    time.sleep(.5)
    clear()
    print("Get Ready 🟩🟥🟥")
    time.sleep(.5)
    clear()
    print("Get Ready 🟩🟩🟥")
    time.sleep(.5)
    clear()
    print("Get Ready 🟩🟩🟩")
    time.sleep(.5)
    clear()
    time.sleep(.2)

    values = []

    for i in range(size):
        randnum = randrange(10)
        values.append(randnum)
        print(randnum)
        talk(randnum)
        time.sleep(2.0)
        clear()
        time.sleep(1.0)

    print("...")
    time.sleep(2.0)
    clear()

    if mode=='inverse':
        values.reverse()
    elif mode=='croissant':
        values.sort()

    talk(f"Donner les chiffres dans l'ordre {mode}")
    result = []
    while result == []:
        for i in range(size):
            print("Enter the values in the given order:\n")
            value = listen()
            if value >= 0:
                print(f"> {value}")
                time.sleep(0.4)
            else:
                talk("Je n'ai pas compris, veuillez écrire")
                value = input('> ')
                if value == 'r':
                    print("Restartin answer")
                    result = []
                    break
            try:
                value = int(value)
                result.append(value==values[i])
            except ValueError:
                print("Wrong format")
                exit(1)
            clear()
    return result

def show_results(results):
    total_correct = 0
    total_count = 0
    for category,c_results in results.items():
        correct,count = show_results_category(c_results,category)
        total_correct+=correct
        total_count+=count
    print(f"\n 🧾Global Score: {total_correct}/{total_count}")

def show_results_category(results, name):
    print(f"CATEGORY: {name}")
    total_correct = 0
    total_count = 0
    for exercice_results in results:
        count = len(exercice_results)
        correct = sum(exercice_results)
        total_correct+=correct
        total_count+=count
        exercice_color_show = ''.join(['🟩' if x else '🟥' for x in exercice_results]) + f" - {correct}/{count}"
        print(exercice_color_show)
    print(f"Total: {total_correct}/{total_count}")
    return total_correct,total_count

if __name__ == "__main__":

    print('\n### STARTING MEMORY TRAINER ###\n')

    results = {
        'original': [],
        'inverse': [],
        'croissant': [],
    }
    min = 6
    max = 8

    mode = 'original'
    for i in range(min,max):
        results[mode].append(exam(i, mode))

    time.sleep(2.0)
    clear()
    print("### REVERSE MODE ###")
    mode = 'inverse'
    for i in range(min,max):
        results[mode].append(exam(i, mode))
    
    print("### SORTED MODE ###")
    time.sleep(2.0)
    clear()
    mode = 'croissant'
    for i in range(min,max):
        results[mode].append(exam(i, mode))

    show_results(results)

    

    # time.sleep(0.5)
    # clear()



    # print("### RESULTS ###\n")
    # time.sleep(0.5)

    # correct = 0
    # for i in range(quantity):
    #     if values[i] == memorized[i]:
    #         print("🟢", end='')
    #         correct+=1
    #     else:
    #         print("🔴", end = '')
    # print("\n")
    # print(f"Score: {correct}/{quantity}")


