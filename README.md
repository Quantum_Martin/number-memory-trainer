# Number Memory Trainer

This python script features a voice assistant to help you train your memory.
It gives you random numbers and asks you to give them back in a specific order.

It uses speech recognition so you can use only your voice.